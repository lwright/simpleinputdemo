SIMPLE INPUT - DEMO
===============

A Simple Drop In Expanding Chat Input Text View For iOS 7 - DEMO - SIMULATOR ONLY 

** Contact for Full Version **

Simple Input
==============
![alt tag](https://github.com/LoganWright/SimpleInputDemo/blob/master/SimpleInputDemo/TextEntry.gif?raw=true)


SETTING UP
==============

Drag SDK
--------------
![alt tag](https://github.com/LoganWright/SimpleInputDemo/blob/master/SimpleInputDemo/DragSDK.png?raw=true)

Import SimpleInput.h & add Delegate in ViewController.h
--------------

ViewController.h
    
```Obj-C
    #import <UIKit/UIKit.h>
    #import "SimpleInput.h"

    @interface ViewController : UIViewController <SimpleInputDelegate>

    @end
```

Create A SimpleInput Reference In Your ViewController.m
--------------

```Obj-C
#import "ViewController.h"

@interface ViewController ()
{
    SimpleInput * simpleInput;
}
@end

@implementation ViewController

// ... (continues)
```
    
Load Simple Input in viewDidLoad 
--------------

ViewController.m -- viewDidLoad

    simpleInput = [[SimpleInput alloc]init];
    simpleInput.delegate = self;
    [self.view addSubview:simpleInput];

Add Rotation Calls
--------------

*Only necessary if your app supports multiple orientations*

ViewController.m

```Obj-C
    - (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
        [simpleInput willRotate];
    }
    - (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
        [simpleInput isRotating];
    }
    - (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
        [simpleInput didRotate];
    }
```

![alt tag](https://github.com/LoganWright/SimpleInputDemo/blob/master/SimpleInputDemo/Rotation.gif?raw=true)


SETTING UP DELEGATE
==============

Receive Message
--------------

```Obj-C
    - (void) simpleInputNewMessageSent:(NSString *)message {
        NSLog(@"NEW MESSAGE: %@", message);
    }
```

SynchronizedAnimation
--------------
![alt tag](https://github.com/LoganWright/SimpleInputDemo/blob/master/SimpleInputDemo/SynchronousAnimation.gif?raw=true)

```Obj-C
    - (void) simpleInputResizing {
    
        /* DO NOT INCLUDE UI ANIMATIONS IN THIS METHOD */
    
        // --- DEMO --- //
    
        // rise with simple input
        [simpleInput slideView:orangeV withOffset:0];
    
        // rise with offset
        [simpleInput slideView:greenV withOffset:20];
    
        // crunch with simple input
        [simpleInput crunchView:torquoiseV withOffset:0];
    
        // crunch with offset
        [simpleInput crunchView:blueV withOffset:20];
    
        // dynamically resize
        [simpleInput adjustViewDynamically:purpleV withMaximumY:20 maximumHeight:240 andOffset:25];
    
        // -- DEMO END -- //
    
      }
```    

CUSTOMIZATION OPTIONS
==============

Customization options should be implemented BEFORE adding SimpleInput to your view!

```Obj-C
    // **** SIMPLE INPUT **** //
    
    simpleInput = [[SimpleInput alloc]init];
    simpleInput.delegate = self;
    simpleInput.placeholderString = @"<Your Placeholder>";
    
    // ** CUSTOMIZATION -- MUST BE IMPLEMENTED BEFORE ADDING TO SUBVIEW ** //
    //
    // -- Color Scheme
    // simpleInput.customBackgroundColor = ...; // (UIColor *)
    // simpleInput.inputBorderColor = ...; // (UIColor *)
    // simpleInput.sendBtnActiveColor = ...; // (UIColor *)
    // simpleInput.sendBtnInactiveColor = ..; // (UIColor *)
    //
    // -- light blue theme example -- //
    // simpleInput.customBackgroundColor = [UIColor colorWithRed:0.142954 green:0.60323 blue:0.862548 alpha:1];
    // simpleInput.sendBtnInactiveColor = [UIColor colorWithWhite:1 alpha:.5];
    // simpleInput.sendBtnActiveColor = [UIColor whiteColor];
    // simpleInput.inputBorderColor = [UIColor clearColor];
    // -- end light blue theme example -- //
    //
    // -- Custom Keyboard
    // simpleInput.customKeyboard = ...; // UIKeyboardAppearance
    //
    // -----------------------------------------------------------------------
    
    // ** OPTIONAL BEHAVIOR -- MUST BE IMPLEMENTED BEFORE ADDING TO SUBVIEW ** //
    //
    // -- Max Y
    // The maximum point on the Y axis that simpleInput can extend to - default: 60
    // simpleInput.maxY = [NSNumber numberWithInt:60];
    //
    // -- Auto Close -- MUST BE ON TOP OF VIEW HIERARCHY TO PROPERLY INTERCEPT TOUCHES
    // Set to YES to prevent auto close behavior
    // simpleInput.stopAutoClose = YES;
    //
    // -- Max Characters
    // The maximum amount of characters to allow at input
    // default is unrestricted
    // simpleInput.maxCharacters = [NSNumber numberWithInt:140];
    //
    // -----------------------------------------------------------------------
    
    //  *** IMPORTANT NOTES *** //
    //
    // -- DO NOT ADJUST SIMPLE INPUT FRAME
    // -- PLACEMENT - MUST BE ON TOP OF VIEW HIERARCHY TO PROPERLY HANDLE AUTOCLOSE
    // -- IN THE DELEGATE METHOD ' simpleInputResizing ' DO NOT INCLUDE ANIMATIONS
    // -- STARTING HEIGHT IS 40px SIMPLE INPUT WILL BE ON BOTTOM OF VIEW
    //
    // -----------------------------------------------------------------------
    
    [self.view addSubview:simpleInput];
    
    // ---- SIMPLE INPUT END ---- //
```























