//
//  ViewController.h
//  SimpleInputDemo
//
//  Created by Logan Wright on 2/6/14.
//  Copyright (c) 2014 Logan Wright. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleInput.h"

@interface ViewController : UIViewController <SimpleInputDelegate>

@end

