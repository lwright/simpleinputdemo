//
//  ViewController.m
//  SimpleInputDemo
//
//  Created by Logan Wright on 2/6/14.
//  Copyright (c) 2014 Logan Wright. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    SimpleInput * simpleInput;
    
    UIView * orangeV;
    UIView * torquoiseV;
    UIView * greenV;
    UIView * blueV;
    UIView * purpleV;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.clipsToBounds = YES;
    
    // **** SIMPLE INPUT **** //
    
    simpleInput = [[SimpleInput alloc]init];
    simpleInput.delegate = self;
    simpleInput.placeholderString = @"iMessage";
    
    // ** CUSTOMIZATION -- MUST BE IMPLEMENTED BEFORE ADDING TO SUBVIEW ** //
    //
    // -- Customization Options
    // simpleInput.customBackgroundColor = ...; // (UIColor *)
    // simpleInput.inputBorderColor = ...; // (UIColor *)
    // simpleInput.textViewBackgroundColor = ...; // (UIColor *)
    // simpleInput.textViewTextColor = ...; // (UIColor *)
    // simpleInput.placeholderTextColor = ...; // (UIColor *)
    // simpleInput.sendBtnInactiveColor = ...; // (UIColor *)
    // simpleInput.sendBtnActiveColor = ...; // (UIColor *)
    // simpleInput.inputBorderColor = ...; // (UIColor *)
    // simpleInput.customKeyboard = ...; // (UIKeyboardAppearance)
    // simpleInput.placeholderString = ...; // (NSString *)
    //
    // -- light blue theme example -- //
    // simpleInput.customBackgroundColor = [UIColor colorWithRed:0.142954 green:0.60323 blue:0.862548 alpha:1];
    // simpleInput.sendBtnInactiveColor = [UIColor colorWithWhite:1 alpha:.5];
    // simpleInput.sendBtnActiveColor = [UIColor whiteColor];
    // simpleInput.inputBorderColor = [UIColor lightGrayColor];
    // -- end light blue theme example -- //
    //
    // -- soft lounge theme example -- //
    // simpleInput.customBackgroundColor = [UIColor colorWithRed:0.959305 green:0.901052 blue:0.737846 alpha:1];
    // simpleInput.textViewBackgroundColor = [UIColor colorWithRed:109.0/255.0 green:37.0/255.0 blue:38.0/255.0 alpha:.35];
    // simpleInput.textViewTextColor = [UIColor colorWithWhite:1 alpha:.9];//[UIColor colorWithRed:0.959305 green:0.901052 blue:0.737846 alpha:1];
    // simpleInput.placeholderTextColor = [UIColor colorWithWhite:1 alpha:.5];
    // simpleInput.sendBtnInactiveColor = [UIColor colorWithRed:109.0/255.0 green:37.0/255.0 blue:38.0/255.0 alpha:.35];
    // simpleInput.sendBtnActiveColor = [UIColor colorWithRed:109.0/255.0 green:37.0/255.0 blue:38.0/255.0 alpha:1];
    // simpleInput.inputBorderColor = [UIColor lightGrayColor];//[UIColor clearColor];
    // simpleInput.customKeyboard = UIKeyboardAppearanceDark;
    // -- end soft lounge theme example -- //
    //
    // ** ------------------------------------------------------------ ** //
    
    
    // ** OPTIONAL BEHAVIOR -- MUST BE IMPLEMENTED BEFORE ADDING TO SUBVIEW ** //
    //
    // -- Max Y
    // The maximum point on the Y axis that simpleInput can extend to - default: 60
    // simpleInput.maxY = [NSNumber numberWithInt:60];
    //
    // -- Auto Close -- MUST BE ON TOP OF VIEW HIERARCHY TO PROPERLY INTERCEPT TOUCHES
    // Set to YES to prevent auto close behavior
    // simpleInput.stopAutoClose = YES;
    //
    // -- Max Characters
    // The maximum amount of characters to allow at input
    // default is unrestricted
    // simpleInput.maxCharacters = [NSNumber numberWithInt:140];
    //
    // ** ----------------------------------------------------------- ** //
    
    
    //  *** IMPORTANT NOTES *** //
    //
    // -- DO NOT ADJUST SIMPLE INPUT FRAME
    // -- PLACEMENT - MUST BE ON TOP OF VIEW HIERARCHY TO PROPERLY HANDLE AUTOCLOSE
    // -- IN THE DELEGATE METHOD ' simpleInputResizing ' DO NOT INCLUDE ANIMATIONS
    // -- STARTING HEIGHT IS 40px SIMPLE INPUT WILL BE ON BOTTOM OF VIEW
    //
    // -----------------------------------------------------------------------
    
    [self.view addSubview:simpleInput];
    
    // ---- SIMPLE INPUT END ---- //
    
    
    
    // ** -------------------------------------------------------------- ** //
    
    
    
    // ---- DEMO --- //
    
    // will slide with simple input
    orangeV = [[UIView alloc]init];
    orangeV.frame = CGRectMake(10 , simpleInput.frame.origin.y, 52, -200);
    orangeV.backgroundColor = [UIColor colorWithRed:255 / 255 green:162.0 / 255.0 blue:0 alpha:1];
    [self.view insertSubview:orangeV belowSubview:simpleInput];
    
    // will slide with simple input | 35 px offset
    greenV = [[UIView alloc]init];
    greenV.frame = CGRectMake(72, simpleInput.frame.origin.y - 35, 52, -(self.view.bounds.size.height - simpleInput.bounds.size.height - 55));
    greenV.backgroundColor = [UIColor colorWithRed:0 green:160.0/255.0 blue:62.0/255.0 alpha:1];
    greenV.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view insertSubview:greenV belowSubview:simpleInput];
    
    // will crunch with simple input
    torquoiseV = [[UIView alloc]init];
    torquoiseV.frame = CGRectMake(134, simpleInput.frame.origin.y, 52, - (self.view.bounds.size.height - simpleInput.bounds.size.height - 60));
    torquoiseV.backgroundColor = [UIColor colorWithRed:36.0/255.0 green:168.0/255.0 blue:172.0/255.0 alpha:1];
    torquoiseV.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view insertSubview:torquoiseV belowSubview:simpleInput];
    
    // will crunch with simple input | 20 px offset
    blueV = [[UIView alloc]init];
    blueV.frame = CGRectMake(196, simpleInput.frame.origin.y - 20, 52, -(self.view.bounds.size.height - simpleInput.bounds.size.height - 120));
    blueV.backgroundColor = [UIColor colorWithRed:0 green:135.0/255.0 blue:203.0/255.0 alpha:1];
    blueV.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view insertSubview:blueV belowSubview:simpleInput];
    
    // will adjust dynamically
    purpleV = [[UIView alloc]init];
    purpleV.frame = CGRectMake(258, simpleInput.frame.origin.y - 25, 52, -(self.view.bounds.size.height - simpleInput.bounds.size.height - 200));
    purpleV.backgroundColor = [UIColor colorWithRed:152.0/255.0 green:35.0/255.0 blue:149.0/255.0 alpha:1];
    purpleV.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.view insertSubview:purpleV belowSubview:simpleInput];
    
    // -- DEMO END -- //
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// *** REQUIRED IF USING MULTIPLE ORIENTATIONS *** //

#pragma mark SIMPLE INPUT ROTATION CALLS

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [simpleInput willRotate];
}
- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [simpleInput isRotating];
}
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [simpleInput didRotate];
}

//  ---------------------------------------------- //

#pragma mark SIMPLE INPUT DELEGATE

// -- REQUIRED!
- (void) simpleInputNewMessageSent:(NSString *)message {
    NSLog(@"NEW MESSAGE: %@", message);
}

// -- OPTIONAL
- (void) simpleInputResizing {
    
    /* DO NOT INCLUDE UI ANIMATIONS IN THIS METHOD */
    
    // --- DEMO --- //
    
    // rise with simple input
    [simpleInput slideView:orangeV withOffset:0];
    
    // rise with offset
    [simpleInput slideView:greenV withOffset:20];
    
    // crunch with simple input
    [simpleInput crunchView:torquoiseV withOffset:0];
    
    // crunch with offset
    [simpleInput crunchView:blueV withOffset:20];
    
    // dynamically resize
    [simpleInput adjustViewDynamically:purpleV withMaximumY:20 maximumHeight:240 andOffset:25];
    
    // -- DEMO END -- //
    
}

@end
