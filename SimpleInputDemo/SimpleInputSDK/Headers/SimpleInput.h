//
//  SimpleInput.h
//  SimpleInput
//
//  Created by Logan Wright on 2/6/14.
//  Copyright (c) 2014 Logan Wright. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SimpleInputDelegate

// CALLED FOR MESSAGE SEND
/*!
 The user has sent a message
 */
- (void) simpleInputNewMessageSent:(NSString *)message;

@optional

// USED FOR SYNCHRONIZING ANIMATION
/*!
 SimpleInput is Resizing - Use to Synchronize Animations - Calling simpleInput.frame will give you the target frame - add any code you'd like to include in simpleInput adjustments.  Do NOT include any UIView Animations.
 */
- (void) simpleInputResizing;

// STATUS CALLS
- (void) simpleInputDidBeginEditing;
- (void) simpleInputDidEndEditing;

@end

@interface SimpleInput : UIView <UITextViewDelegate>

// -- DELEGATE -- //
/*!
 Delegate
 */
@property (retain, nonatomic) id<SimpleInputDelegate>delegate;

// -- CUSTOMIZATION OPTIONS -- //
/*!
 Placeholder String
 */
@property (strong, nonatomic) NSString * placeholderString;
/*!
 The color of simpleInput
 */
@property (strong, nonatomic) UIColor * customBackgroundColor;
/*!
 The color of the input border - 1 px - defaults to no border
 */
@property (strong, nonatomic) UIColor * inputBorderColor;
/*!
 The color of the send btn - Active State;
 */
@property (strong, nonatomic) UIColor * sendBtnActiveColor;
/*!
 The color of the send btn - Inactive State;
 */
@property (strong, nonatomic) UIColor * sendBtnInactiveColor;
/*!
 The color of the message text;
 */
@property (strong, nonatomic) UIColor * textViewTextColor;
/*!
 The color of the message field background;
 */
@property (strong, nonatomic) UIColor * textViewBackgroundColor;
/*!
 The color of the placeholder text - defaults to light gray;
 */
@property (strong, nonatomic) UIColor * placeholderTextColor;
/*!
 The custom keyboard to use
 */
@property UIKeyboardAppearance customKeyboard;


// -- BEHAVIOR CUSTOMIZATION -- //

/*!
 The maximum point on the Y axis that simpleInput can extend to - default: 60
 */
@property (strong, nonatomic) NSNumber * maxY;

/*!
 Set to YES to prevent auto close behavior
 */
@property BOOL stopAutoClose;

/*!
 Maximum character count allowed
 */
@property (strong, nonatomic) NSNumber * maxCharacters;

// -- RESIZING METHODS -- //
/*!
 Slides view vertically - Maintains View's Bounds.
 @param viewToMove Any view you'd like to slide
 @param offset The distance between the bottom of the view and the top of simpleInput
 @result nil
 */
- (void) slideView:(UIView *)viewToSlide withOffset:(int)offset;
/*!
 Crunches view vertically - Maintains View's Origin
 @param viewToCrunch Any view you'd like to crunch
 @param offset The distance between the bottom of the view and the top of simpleInput
 */
- (void) crunchView:(UIView *)viewToCrunch withOffset:(int)offset;
/*!
 Slides view vertically until it reaches minY, then Crunches
 @param viewToAdjust Any view you'd like to adjust
 @param maximumY Point on Y axis where the view begins to crunch
 @param maxHeight Maximum height of the view
 @param offset the distance between the bottom of the view and the top of simpleInput
 */
- (void) adjustViewDynamically:(UIView *)viewToAdjust withMaximumY:(int)maximumY maximumHeight:(int)maxHeight andOffset:(int)offset;

// -- CLOSING | OPENING -- //
/*!
 Closes keyboard and resigns first responder
 */
- (void) close;
/*!
 Opens keyboard and makes simple input first responder.
 */
- (void) open;

// -- CALLED DURING ROTATION -- //
/*!
 Should ONLY be called in Parent View Controller's Rotation Callbacks - SEE DOCS
 */
- (void) willRotate;
/*!
 Should ONLY be called in Parent View Controller's Rotation Callbacks - SEE DOCS
 */
- (void) isRotating;
/*!
 Should ONLY be called in Parent View Controller's Rotation Callbacks - SEE DOCS
 */
- (void) didRotate;




/////////////////// ******** MUST IMPLEMENT FOR ROTATION ******** ///////////////////

// -- Copy these methods into simpleInput's parent view controller

/* ---------------------------------------------
 
 - (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
 [simpleInput willRotate];
 }
 - (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
 [  simpleInput isRotating];
 }
 - (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
 [simpleInput didRotate];
 }
 
 --------------------------------------------- */




/////////////////// ******** IMPLEMENTATION EXAMPLES ******** ///////////////////

///// **** RESIZING - DO NOT INCLUDE UI ANIMATIONS IN RESIZING **** /////

// -- Declared in simpleInput delegate

/* ---------------------------------------------
 
 - (void) simpleInputResizing {
 
 // rise with simple input
 [simpleInput slideView:orangeV withOffset:0];
 
 // rise with offset
 [simpleInput slideView:greenV withOffset:20];
 
 // crunch with simple input
 [simpleInput crunchView:torquoiseV withOffset:0];
 
 // crunch with offset
 [simpleInput crunchView:blueV withOffset:20];
 
 // dynamically resize
 [simpleInput adjustViewDynamically:purpleV withMaximumY:20 maximumHeight:240 andOffset:25];
 
 }
 
 --------------------------------------------- */


///// **** CALLING SIMPLE INPUT **** /////

// -- Declared in viewDidLoad - or wherever you'd like to load simpleInput

/* ---------------------------------------------
 
 simpleInput = [[SimpleInput alloc]init];
 simpleInput.delegate = self;
 
 // ** CUSTOMIZATION -- MUST BE IMPLEMENTED BEFORE ADDING TO SUBVIEW ** //
 //
 // -- Color Scheme
 // simpleInput.customBackgroundColor = ...; // (UIColor *)
 // simpleInput.inputBorderColor = ...; // (UIColor *)
 // simpleInput.sendBtnActiveColor = ...; // (UIColor *)
 // simpleInput.sendBtnInactiveColor = ..; // (UIColor *)
 //
 // -- Custom Keyboard
 // simpleInput.customKeyboard = ...; // UIKeyboardAppearance
 //
 // -----------------------------------------------------------------------
 
 // ** OPTIONAL BEHAVIOR -- MUST BE IMPLEMENTED BEFORE ADDING TO SUBVIEW ** //
 //
 // -- Max Y
 // The maximum point on the Y axis that simpleInput can extend to - default: 60
 // simpleInput.maxY = [NSNumber numberWithInt:60];
 //
 // -- Auto Close -- MUST BE ON TOP OF VIEW HIERARCHY TO PROPERLY INTERCEPT TOUCHES
 // Set to YES to prevent auto close behavior
 // simpleInput.stopAutoClose = YES;
 //
 // -- Max Characters
 // The maximum amount of characters to allow at input
 // default is unrestricted
 // simpleInput.maxCharacters = [NSNumber numberWithInt:140];
 //
 // -----------------------------------------------------------------------
 
 //  *** IMPORTANT NOTES *** //
 //
 // -- DO NOT ADJUST SIMPLE INPUT FRAME
 // -- PLACEMENT - MUST BE ON TOP OF VIEW HIERARCHY TO PROPERLY HANDLE AUTOCLOSE
 // -- IN THE DELEGATE METHOD ' simpleInputResizing ' DO NOT INCLUDE ANIMATIONS
 // -- STARTING HEIGHT IS 40px SIMPLE INPUT WILL BE ON BOTTOM OF VIEW
 //
 // -----------------------------------------------------------------------
 
 [self.view addSubview:simpleInput];
 
 --------------------------------------------- */

@end